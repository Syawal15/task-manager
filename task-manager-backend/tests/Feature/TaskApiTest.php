<?php

namespace Tests\Feature;

use Tests\TestCase;
use App\Models\Task;
use App\Models\User;
use Laravel\Passport\Passport;

class TaskApiTest extends TestCase
{
    /**
     * A basic feature test example.
     *
     * @return void
     */
    protected $user;

     public function setUp(): void
    {
        parent::setUp();

        $this->user = User::factory()->create();
        Passport::actingAs($this->user);
    }

    protected function getAccessToken()
    {
        return Passport::actingAs($this->user)->accessToken;
    }

    public function test_can_create_task()
    {
        $accessToken = $this->getAccessToken();

        $taskData = [
            'title' => 'Tugas Baru',
            'description' => 'Deskripsi tugas baru',
            'status' => 'todo',
            'user_id' => '2'
        ];

        $response = $this->withHeaders([
            'Authorization' => "Bearer $accessToken", // Menyertakan token akses dalam header
        ])->post('/api/tasks', $taskData);

        $response->assertStatus(201); 
        $response->assertJsonStructure([
            'id',
            'title',
            'description',
            'status',
            'user_id'
        ]);
    }

    public function test_can_update_task()
    {
        $accessToken = $this->getAccessToken();

        $task = Task::factory()->create();

        $updatedData = [
            'title' => 'Judul Diperbarui',
            'status' => 'in-progress',
            'user_id' => '1'
        ];

        $response = $this->withHeaders([
            'Authorization' => "Bearer $accessToken", // Menyertakan token akses dalam header
        ])->put("/api/tasks/{$task->id}", $updatedData);

        $response->assertStatus(200);
        $response->assertJson($updatedData);
    }

    public function test_can_get_tasks()
    {
        $accessToken = $this->getAccessToken();

        Task::factory()->create();

        $response = $this->withHeaders([
            'Authorization' => "Bearer $accessToken", 
        ])->get('/api/tasks');

        $response->assertStatus(200);
        $response->assertJsonStructure([
            'data' => [
                '*' => [
                    'id',
                    'title',
                    'description',
                    'status',
                    'user_id'
                ],
            ],
            'current_page',
            'last_page',
        ]);
    }

    public function test_can_delete_task()
    {
        $accessToken = $this->getAccessToken();

        $task = Task::factory()->create();

        $response = $this->withHeaders([
            'Authorization' => "Bearer $accessToken", // Menyertakan token akses dalam header
        ])->delete("/api/tasks/{$task->id}");

        $response->assertStatus(204);
        $this->assertDatabaseMissing('tasks', ['id' => $task->id]); 
    }
}
