<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateTasksTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('tasks', function (Blueprint $table) {
            $table->id(); // Kolom ID otomatis
            $table->string('title'); // Kolom untuk judul tugas
            $table->text('description')->nullable(); // Kolom untuk deskripsi tugas (boleh kosong)
            $table->enum('status', ['todo', 'in-progress', 'completed'])->default('todo'); // Kolom untuk status tugas dengan nilai default 'todo'
            $table->timestamps(); // Kolom tanggal pembuatan dan pembaruan otomatis
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('tasks');
    }
}
