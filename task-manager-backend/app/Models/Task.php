<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Task extends Model
{
    use HasFactory;

    protected $fillable = ['title', 'description', 'status', 'user_id'];

    // Definisikan nilai-nilai status yang valid
    public static $validStatuses = ['todo', 'in-progress', 'completed'];

    public function user()
    {
        return $this->belongsTo(User::class);
    }
}
