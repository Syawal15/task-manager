<?php

namespace App\Http\Controllers;

use App\Models\Task;
use App\Models\User;
use Illuminate\Http\Request;

class TaskController extends Controller
{
    public function index(Request $request)
    {
        $sort = $request->input('sort', 'created_at'); // Default pengurutan berdasarkan created_at
        $status = $request->input('status', ''); // Status default kosong (tanpa penyaringan)
        $perPage = $request->input('per_page', 10); // Jumlah tugas per halaman (default 10)

        $user = auth()->user(); // Mengecek user yang sedang login

        $query = Task::where('user_id', $user->id); // Hanya tugas yang terkait dengan pengguna yang login

        if (!empty($status)) {
            $query->where('status', $status);
        }

        $tasks = $query->orderBy($sort)->paginate($perPage);
        
        return response()->json($tasks);
    }

    public function store(Request $request)
    {
        $validatedData = $request->validate([
            'title' => 'required|max:255',
            'description' => 'nullable',
            'status' => 'required|in:todo,in-progress,completed',
            'user_id' => 'required',
        ]);

        $task = Task::create($validatedData);
        return response()->json($task, 201);
    }

    public function show(Task $task)
    {
        return response()->json($task);
    }

    public function update(Request $request, Task $task)
    {
        $validatedData = $request->validate([
            'title' => 'required|max:255',
            'description' => 'nullable',
            'status' => 'required|in:todo,in-progress,completed',
            'user_id' => 'required',
        ]);

        $task->update($validatedData);
        return response()->json($task);
    }

    public function destroy(Task $task)
    {
        $task->delete();
        return response()->json(null, 204);
    }

    public function getUsers()
    {
        $users = User::all(); // Mengambil semua pengguna dari tabel users
        return response()->json($users);
    }
}
